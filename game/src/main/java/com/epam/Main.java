package com.epam;

import com.epam.entity.AbstractFighter;
import com.epam.entity.NiceFighter;
import com.epam.entity.SmartFighter;
import com.epam.ring.Championship;
import com.epam.ring.Fight;

public class Main {
    public static void main(String[] args) {
        AbstractFighter superMan = new NiceFighter(1, "SuperMan", 1000, 200, 65);
        AbstractFighter batMan = new SmartFighter(2, "BatMan", 2000, 100, 135);
        Fight fight = new Championship();
        fight.battle(superMan, batMan);
    }
}
