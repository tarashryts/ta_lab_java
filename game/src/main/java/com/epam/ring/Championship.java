package com.epam.ring;

import com.epam.entity.AbstractFighter;
import java.util.Random;
import org.apache.log4j.Logger;

public class Championship implements Fight {
    private static final Logger logger = Logger.getLogger(Championship.class);
    @Override
    public void battle(AbstractFighter firstFighter, AbstractFighter secondFighter) {
        logger.info("Fight started!");
        while (true) {
            if (smite(firstFighter, secondFighter).equalsIgnoreCase("game over")) {
                System.out.printf("%s WIN", firstFighter.getName().toUpperCase());
                break;
            }
            if (smite(secondFighter, firstFighter).equalsIgnoreCase("game over")) {
                System.out.printf("%s WIN", secondFighter.getName().toUpperCase());
                break;
            }
        }
    }

    private String smite(AbstractFighter firstFighter, AbstractFighter secondFighter) {
        Random random = new Random();
        int smite = random.nextInt(firstFighter.getMaxAttack());
        int block = random.nextInt(secondFighter.getMaxBlock());
        int damage = Math.max(smite - block, 0);
        if (secondFighter.getHealth() - damage <= 0) {
            System.out.printf("%s hit %s, damage: %d\n",
                    firstFighter.getName(),
                    secondFighter.getName(),
                    smite - block);
            return "Game Over";
        } else {
            secondFighter.setHealth(secondFighter.getHealth() - damage);
            System.out.printf("%s hit %s, damage: %d\n",
                    firstFighter.getName(),
                    secondFighter.getName(),
                    damage);
            System.out.printf("%s health - %d\n",
                    secondFighter.getName(),
                    secondFighter.getHealth());
            return "The Battle Continues";
        }
    }
}
