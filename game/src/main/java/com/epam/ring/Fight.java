package com.epam.ring;

import com.epam.entity.AbstractFighter;

public interface Fight {
    void battle(AbstractFighter firstFighter, AbstractFighter secondFighter);
}
