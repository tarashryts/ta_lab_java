package com.epam.entity;

public abstract class AbstractFighter {
    protected int id;
    protected String name;
    protected int health;
    protected int maxAttack;
    protected int maxBlock;

    public AbstractFighter(int id, String name, int health, int maxAttack, int maxBlock) {
        this.id = id;
        this.name = name;
        this.health = health;
        this.maxAttack = maxAttack;
        this.maxBlock = maxBlock;
    }

    public String getName() {
        return name;
    }
    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxAttack() {
        return maxAttack;
    }
    public int getMaxBlock() {
        return maxBlock;
    }
}
