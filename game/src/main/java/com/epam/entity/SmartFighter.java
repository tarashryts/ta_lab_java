package com.epam.entity;

import java.util.Random;
import org.apache.log4j.Logger;

public class SmartFighter extends AbstractFighter {
    private static final Logger logger = Logger.getLogger(SmartFighter.class);
    public SmartFighter(int id, String name, int health, int maxAttack, int maxBlock) {
        super(id, name, health, maxAttack, maxBlock);
        logger.info("Fighter " + this.getClass().getName() + " created");
    }
}
