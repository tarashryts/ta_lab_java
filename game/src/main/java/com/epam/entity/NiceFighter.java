package com.epam.entity;

import org.apache.log4j.Logger;

public class NiceFighter extends AbstractFighter {
    private static final Logger logger = Logger.getLogger(NiceFighter.class);
    public NiceFighter(int id, String name, int health, int maxAttack, int maxBlock) {
        super(id, name, health, maxAttack, maxBlock);
        logger.info("Fighter " + this.getClass().getName() + " created");
    }
}
