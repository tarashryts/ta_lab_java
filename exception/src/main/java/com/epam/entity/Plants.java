package com.epam.entity;

import com.epam.Main;
import com.epam.enums.Color;
import com.epam.enums.Type;
import org.apache.log4j.Logger;

public class Plants {
    private static final Logger logger = Logger.getLogger(Plants.class);
    int size;
    Color color;
    Type type;

    public Plants(int size, Color color, Type type) {
        this.size = size;
        this.color = color;
        this.type = type;
        logger.info("Plant is created " + this.getClass().getName());
    }
}
