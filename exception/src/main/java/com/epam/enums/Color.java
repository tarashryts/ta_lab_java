package com.epam.enums;

public enum Color {
    GREEN, BLUE, YELLOW;

    @Override
    public String toString() {
        return "Color{}";
    }
}
