package com.epam;

import com.epam.entity.Plants;
import com.epam.enums.Color;
import com.epam.enums.Type;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.log4j.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    private static Plants[] plants = new Plants[] {new Plants(2, Color.BLUE, Type.BIG),
            new Plants(4, Color.GREEN, Type.BIG),
            new Plants(6, Color.YELLOW, Type.SMALL),
            new Plants(8, Color.GREEN, Type.SMALL),
            new Plants(1, Color.YELLOW, Type.BIG)};


    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        logger.info("Enter width: ");
        int a = 0;
        try {
            a = Integer.parseInt(bufferedReader.readLine());
        } catch (IOException e) {
            logger.error("You should input integer value ");
        }
        int b = 5;
        int result = squareRectangle(a, b);
        logger.info("Area of rectangle = " + result);
    }

    public static int squareRectangle(int a, int b) {
        if ((a > 0) && (b > 0)) {
            return a * b;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
