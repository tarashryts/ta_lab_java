package com.epam.exception;

public class ColorException extends RuntimeException {
    public ColorException(String message) {
        super(message);
    }
}
