package com.epam.models.employee;

public abstract class AbstractEmployee {
    private int id;
    private String name;
    private int age;

    public AbstractEmployee() {
    }

    public AbstractEmployee(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public void eat() {
        System.out.println("Somebody is eating");
    }

    public abstract void work();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
