package com.epam.models.employee;

import org.apache.log4j.Logger;

public class Worker extends AbstractEmployee implements ActionsInShop{
    private static final Logger logger = Logger.getLogger(Worker.class);

    @Override
    public void eat() {
        logger.info("Worker should eat fast...");
    }

    public void work() {
        logger.info("Worker is working...");
    }

    @Override
    public void unpackProduct() {
        logger.info("Worker is unpacking product...");
    }
}
