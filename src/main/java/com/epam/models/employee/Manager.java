package com.epam.models.employee;

import org.apache.log4j.Logger;

public class Manager extends AbstractEmployee implements ActionsInShop, ActionsManaging {
    private static final Logger logger = Logger.getLogger(Manager.class);
    private String department;

    public Manager() {
    }

    public Manager(int id, String name, int age, String department) {
        super(id, name, age);
        this.department = department;
    }

    public void work() {
        logger.info("Manager is working...");
        doSomeSecretWork();
        doneSomeSecretWork();
    }

    @Override
    public void unpackProduct() {
        logger.info("Manager is unpacking...");
    }

    @Override
    public void bringToStorage() {
        logger.info("Manager bring something to storage...");
    }

    @Override
    public void manage() {
        logger.info("Manager is managing....");
    }

    private void doSomeSecretWork() {
        logger.info("I'm doing some secret work...(private)");
    }

    private void doneSomeSecretWork() {
        logger.info("I done my secret work...(private)");
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
