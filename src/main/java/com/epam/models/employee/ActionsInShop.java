package com.epam.models.employee;

public interface ActionsInShop {
    void unpackProduct();

    default void bringToStorage() {
        System.out.println("Somebody bring product to storage..");
    }
}
