package com.epam.models.employee;

import org.apache.log4j.Logger;

public class Director extends AbstractEmployee implements ActionsManaging {
    private static final Logger logger = Logger.getLogger(Director.class);
    private int experience;

    public Director() {
    }

    public Director(int experience) {
        this.experience = experience;
    }

    public Director(int id, String name, int age, int experience) {
        super(id, name, age);
        this.experience = experience;
    }

    public void work() {
        logger.info("Director is working...");
    }

    @Override
    public void manage() {
        logger.info("Director is managing...");
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}
