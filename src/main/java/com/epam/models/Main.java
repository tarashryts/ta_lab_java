package com.epam.models;

import com.epam.models.employee.AbstractEmployee;
import com.epam.models.employee.Director;
import com.epam.models.employee.Manager;
import com.epam.models.employee.Worker;

public class Main {
    public static void main(String[] args) {
        AbstractEmployee worker = new Worker();
        AbstractEmployee manager = new Manager();
        AbstractEmployee director = new Director();
        worker.work();
        manager.work();
        director.work();
        worker.eat();
        manager.eat();
        director.eat();
    }
}
